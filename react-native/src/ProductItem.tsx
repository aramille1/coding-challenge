import React from 'react'
import {Text, View,FlatList, Image, StyleSheet } from 'react-native'
import Moment from 'moment';
import newImg from "../assets/new.png"

export default function ProductItem(invData: { inventory: any[]}) {
    const renderHeader = (item: { fields: { [x: string]: string; }; }) =>{
        if(item.fields["Product Name"]){
            if(item.fields["Product Name"].length < 16){
                return item.fields["Product Name"].split(' ').slice(0,2).join(' ')
            }else{
                return item.fields["Product Name"].split(' ').slice(0,1).join(' ')
            }
        }
    }

    const renderTags = (item: { fields: { [x: string]: string; }; }) => {
        if(item.fields["Product Categories"]){
            return item.fields["Product Categories"]
            .split(",")
            .map((item => {
                if(item.includes(':')){
                    const tempItem = item.split(":").slice(1).join('.')
                    return <Text style={styles.itemTagsText}>{tempItem}</Text>
                }
                return <Text style={styles.itemTagsText}>{item}</Text>
            }))
        }else{
            return null
        }
    }

    const renderNewImg = (item: { createdTime: string | number | Date; }) =>{
        const today = new Date().getTime();
        const productsDate = new Date(item.createdTime).getTime();
        const dayInMS = 24 * 3600 * 1000;
        const diffInMilliseconds = today - productsDate;
        const diffInDays = diffInMilliseconds / dayInMS
        if(Math.round(diffInDays) <= 7){
            return <Image style={styles.newImg} source={newImg}/>
        }        
    }
    
  return (
    <View>
    <FlatList
      data={invData.inventory}
      keyExtractor={(item, index) => index.toString()}
      renderItem={({item}) => 
           <View style={styles.itemWrap} key={item.id}>
                <View style={styles.itemImgWrap}>
                    <Image style={styles.itemImg} source={{uri: item.fields["Product Image"] ? item.fields["Product Image"] : "https://complianz.io/wp-content/uploads/2019/03/placeholder-300x202.jpg"}}/> 
                </View>
                <View style={styles.itemHeader}>
                    <Text style={styles.itemHeader_h2}>{renderHeader(item)} </Text>
                    <Text>{Moment(item.fields.Posted).format('DD.MM.YYYY')}</Text>
                    <View style={styles.itemHeaderTags}>
                        {renderTags(item)}
                    </View>
                </View>
                {renderNewImg(item)}
           </View>
    }></FlatList>
   </View>
  )
}

const styles = StyleSheet.create({
    itemWrap: {
        flex:1,
        flexDirection: 'row',
        backgroundColor: '#F8F9FC',
        padding: 10,
        margin: 10,
        borderRadius: 5,
        shadowColor: '#1b263340',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.25,
        shadowRadius: 2,  
        elevation: 5
    },
    itemImgWrap:{
        width: 83,
        height: 112,
    },
    itemImg: {
        flex: 1
    },
    itemHeaderTags: {
        marginTop: 10,
        paddingRight: 10,
        flex:1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: '90%',
    },
    itemTagsText: {
        textAlign: 'center',
        backgroundColor: '#D5E6FF',
        borderRadius: 40,
        paddingVertical: 7,
        paddingHorizontal: 12,
        marginRight: 10,
        marginBottom: 10
    },
    itemHeader:{
        marginLeft: 20,
    },
    itemHeader_h2: {
        fontWeight: "bold",
        fontSize: 20,
        lineHeight: 22,
        marginBottom: 5,
    },
    newImg:{
        position: 'absolute',
        top:0,
        right: 0,
        margin: 8
    }
    
  });